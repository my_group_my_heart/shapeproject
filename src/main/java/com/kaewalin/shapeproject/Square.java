/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kaewalin.shapeproject;

/**
 *
 * @author ACER
 */
public class Square {
    private double side;
    public Square(double side){
        this.side = side;
    }
    public double squareArea(){
        return side*side;
    }
      public double getS(){
        return side;  
      }
      public void setS(double side){
          if(side <= 0){
              System.out.println("Error: Radius must more than zero!!!");
              return;
          }
          this.side = side;
      }
       @Override
    public String toString(){
      return ("Side = "+this.getS()+", SquareArea = "+this.squareArea());            
      
    }
}
