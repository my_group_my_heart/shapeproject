/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kaewalin.shapeproject;

/**
 *
 * @author ACER
 */
public class Triangle {
    private double base,high;
    public Triangle(double base,double high){
        this.base = base;
        this.high = high;
    }
    public double triaArea(){
        return this.base*this.high/2;
    }
    public double getB(){
        return this.base;
    }
    public double getH(){
        return this.high;
    }
    public void setBH(double base,double high){
       if(base<=0 || high<=0 ){
           System.out.println("Error: Radius must more than zero!!!");
           return;
       }
        this.base = base;
        this.high = high;
    }
    @Override
    public String toString(){
        return "Base = "+this.getB()+" , "+"High = "+this.getH()+", TriangleArea = "+this.triaArea();
    
        
        
    }
}
